<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Avatud Kool</title>

  <?php get_template_part('partials/favicons'); ?>

  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

  <header class="global-header">
    <nav class="global-menu">
      <div class="wrap">
        <button class="menu-toggle js-menu-trigger">Menüü</button>
      <div class="menu-wrapper">

      <?php

      $global_menu = array(
      	'theme_location'  => 'global_header_nav',
      	'container'       => false,
      	'menu_class'      => 'menu',
      	'echo'            => true,
      	'fallback_cb'     => 'wp_page_menu',
      	'depth'           => 1
      );

      wp_nav_menu( $global_menu );

      ?>
    </div>
    <?php language_selector(); ?>
      </div> <!-- /.wrap -->
    </nav>


  </header>
