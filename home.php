<?php get_header(); ?>

<?php get_template_part('partials/hero'); ?>

<div class="wrap">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('post-body'); ?>>

    <header class="post-header">
      <h1 class="post-header__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

    </header>
    <div class="post-excerpt body-copy">
      <?php the_excerpt(); ?>
    </div>
    <footer class="post-meta">
      <span>
        <time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('d.m.Y'); ?></time>
        <span class="post-meta__author"><?php the_author_meta( 'display_name' ); ?></span>
      </span>
    </footer>

  </article>
<?php endwhile; ?>
  <!-- post navigation -->
<?php else: ?>
  <!-- no posts found -->
<?php endif; ?>

</div>

<?php get_footer(); ?>
