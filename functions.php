<?php

// Enqueue scripts and styles
function enqueue_scripts_styles() {
  $styles_url = get_template_directory_uri().'/styles/css/global.css';
  $scripts_url = get_template_directory_uri().'/scripts/scripts.js';
  $svg_injector_url = get_template_directory_uri().'/scripts/vendor/svg-injector.js';
  $google_fonts = '//fonts.googleapis.com/css?family=Dosis:400,700|Roboto+Condensed:400,700&subset=cyrillic,cyrillic-ext,latin-ext';

  wp_register_style('fonts', $google_fonts);
  wp_register_script( 'svg-injector', $svg_injector_url, '', '1.1.3', true );
  wp_enqueue_style('global-styles', $styles_url, array('fonts'), '1.0.0', 'screen');
  wp_enqueue_script('global-scripts', $scripts_url, array('jquery', 'svg-injector'), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'enqueue_scripts_styles');

function theme_setup() {

  register_nav_menus(array(
    'global_header_nav'  => __('Global Header Menu', 'ak-theme'),
  ));

}
add_action('after_setup_theme', 'theme_setup');

function island_shortcode($atts, $content = null) {
  return '<div class="card">' . $content . '</div>';
}
add_shortcode( 'card', 'island_shortcode' );



function button_shortcode($atts, $content = null) {

  extract( shortcode_atts(
    array(
      'url' => null
    ), $atts )
  );

  return '<a href="'.$url.'" class="button">' . $content . '</a>';
}
add_shortcode( 'button', 'button_shortcode' );





add_filter( 'gform_field_input', 'inline_text', 10, 5 );
function inline_text( $input, $field, $value, $lead_id, $form_id ) {
  if ($form_id == 1):

    if ( $field->id == 6 ) {
        $input = 'Olen';
    }
    if ( $field->id == 7 ) {
        $input = 'ja olen huvitatud';
    }
    if ( $field->id == 8 ) {
        $input = '.';
    }
    if ( $field->id == 9 ) {
        $input = 'Minu nimi on';
    }
    if ( $field->id == 10 ) {
        $input = 'ja e-post';
    }
    if ( $field->id == 11 ) {
        $input = 'ning';
    }
    if ( $field->id == 12 ) {
        $input = 'suhtlemiseks eelistan';
    }
    if ( $field->id == 15 ) {
        $input = '.';
    }
    if ( $field->id == 14 ) {
        $input = 'Aidata saaksin teid:';
    }

  endif;
  return $input;
}








//  icon shortcode
//  =========================================================
/*
    [icon img="heart/loupe/mountains/stars"]...[/icon]
*/
//  ----------------------------------------------------------

function icon_shortcode($atts, $content = null) {
// Attributes
  extract( shortcode_atts(
    array(
      'img' => null
    ), $atts )
  );

  if($img == 'heart'){
    $img_content = get_template_directory_uri().'/images/icon-heart.svg';

  } elseif($img == 'loupe') {
    $img_content = get_template_directory_uri().'/images/icon-loupe.svg';

  } elseif($img == 'mountains') {
    $img_content = get_template_directory_uri().'/images/icon-mountains.svg';

  } elseif($img == 'stars') {
    $img_content = get_template_directory_uri().'/images/icon-stars.svg';

  } elseif($img == 'speech') {
    $img_content = get_template_directory_uri().'/images/icon-speech.svg';

  } elseif($img == 'boxes') {
    $img_content = get_template_directory_uri().'/images/icon-boxes.svg';

  } elseif($img == 'network') {
    $img_content = get_template_directory_uri().'/images/icon-network.svg';

  } elseif($img == 'pencilruler') {
    $img_content = get_template_directory_uri().'/images/icon-pencil-ruler.svg';

  } elseif($img == 'lab') {
    $img_content = get_template_directory_uri().'/images/icon-lab.svg';

  } elseif($img == 'glasses') {
    $img_content = get_template_directory_uri().'/images/icon-glasses.svg';

  } elseif($img == 'feather') {
    $img_content = get_template_directory_uri().'/images/icon-feather.svg';

  } elseif($img == 'tree') {
    $img_content = get_template_directory_uri().'/images/icon-tree.svg';

  } elseif($img == 'scales') {
    $img_content = get_template_directory_uri().'/images/icon-scales.svg';

  } else {
    $img_content = null;
  }

  return '<img class="icon" src="'. $img_content .'">';
}

add_shortcode( 'icon', 'icon_shortcode' );








//	Deco
//	=========================================================
/*
		[deco]...[/deco]
*/
//	----------------------------------------------------------

function deco_shortcode($atts, $content = null) {
	return '<div class="deco">' . $content . '</div>';
}
add_shortcode( 'deco', 'deco_shortcode' );





function remove_menus(){

  // remove_menu_page( 'index.php' );                  //Dashboard
  // remove_menu_page( 'edit.php' );                   //Posts
  // remove_menu_page( 'upload.php' );                 //Media
  // remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  // remove_menu_page( 'themes.php' );                 //Appearance
  // remove_menu_page( 'plugins.php' );                //Plugins
  // remove_menu_page( 'users.php' );                  //Users
  remove_menu_page( 'tools.php' );                  //Tools
  // remove_menu_page( 'options-general.php' );        //Settings

}

add_action( 'admin_menu', 'remove_menus' );


function remove_dashboard_meta() {
  remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
  remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal');
}
add_action( 'admin_init', 'remove_dashboard_meta' );


function language_selector(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
          echo "<ul class='language-switcher'>";
        foreach($languages as $l){
            if( $l['active'] ):
              echo "<li class='active'>";
            else:
              echo "<li>";
            endif;

            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo $l['native_name'];
            if(!$l['active']) echo '</a>';
            echo "</li>";
        }
          echo "</ul>";
    }
}
