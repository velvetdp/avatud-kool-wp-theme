<?php
// check if the flexible content field has rows of data
if( have_rows('modules') ): ?>

    <?php // loop through the rows of data
    while ( have_rows('modules') ) : the_row(); ?>

      <?php if( get_row_layout() == 'two_columns' ): ?>
        <div class="wrap wrap--twocol-section">
          <article class="twocol-section body-copy">

            <div class="twocol-section__aside">
              <h2><?php the_sub_field('title'); ?></h2>

              <?php the_sub_field('aside_body'); ?>
            </div>

            <div class="twocol-section__body">
            <?php the_sub_field('body'); ?>
            </div> <!-- /.twocol-section__body -->

          </article> <!-- /.twocol-section -->
        </div> <!-- /.wrap -->


      <?php elseif ( get_row_layout() == 'poster' ): ?>

        <div class="poster-section">
          <div class="skew"></div>
          <div class="wrap">
            <h2 class="poster-section__title"><?php the_sub_field('title'); ?></h2>

            <?php the_sub_field('caption'); ?>

          </div>
        </div> <!-- /.poster-section -->

      <?php elseif ( get_row_layout() == 'grid_block_list' ): ?>
        <?php
          $bgimage = get_sub_field('background_image');

          if ( get_sub_field('light_overlay') ):
            $lightoverlay = ' grid-block-section--light ';
          else:
            $lightoverlay = '';
          endif;

         if( !empty($bgimage) ): ?>

        <div class="grid-block-section <?php echo $lightoverlay; ?>" style="background-image:url('<?php echo $bgimage['url']; ?>')">
        <?php else: ?>
        <div class="grid-block-section <?php echo $lightoverlay; ?>">
        <?php endif; ?>
        <div class="skew"></div>

          <div class="wrap">
            <h2 class="grid-block-section--title"><?php the_sub_field('title'); ?></h2>

            <?php $excerpt = get_sub_field('excerpt'); ?>
            <?php if ($excerpt) { ?>
            <div class="grid-block__excerpt body-copy">
              <?php echo $excerpt; ?>
            </div>
            <?php } ?>

            <?php

            $items_count = count( get_sub_field('list') );
            $columns = get_sub_field('columns');
            $grid_modifier = '';

            if ( $columns == 'auto') {


              if ( $items_count == 1 ):
                $grid_modifier = '';
              elseif( $items_count == 2 ):
                $grid_modifier = 'grid-block--two';
              elseif( $items_count == 3 ):
                $grid_modifier = 'grid-block--three';
              elseif( $items_count == 4 ):
                $grid_modifier = 'grid-block--four';
              elseif( $items_count == 5 ):
                $grid_modifier = 'grid-block--five';
              else:
                $grid_modifier = '';
              endif;

            } else {

              if ( $columns == 1 ):
                $grid_modifier = 'grid-block--one';
              endif;
              if( $columns == 2 ):
                $grid_modifier = 'grid-block--two';
              endif;
              if( $columns == 3 ):
                $grid_modifier = 'grid-block--three';
              endif;
              if( $columns == 4 ):
                $grid_modifier = 'grid-block--four';
              endif;
              if( $columns == 5 ):
                $grid_modifier = 'grid-block--five';
              endif;

            }

            ?>

            <?php if( have_rows('list') ): ?>
            <ul class="grid-block <?php echo $grid_modifier; ?>">
            <?php while( have_rows('list') ): the_row(); ?>
              <li>
                <div class="grid-block-section__body body-copy">
                  <?php the_sub_field('content'); ?>
                </div>
              </li>
            <?php endwhile; ?>
            </ul>
          <?php endif; //if( have_rows('list') ): ?>

            <?php if (get_sub_field('button')): ?>

              <?php if ( get_sub_field('light_overlay') ): ?>
                <a href="<?php echo get_sub_field('button_url') ?>" class="button button--grid-block"><?php echo get_sub_field('button_label') ?></a>
              <?php else: ?>
                <a href="<?php echo get_sub_field('button_url') ?>" class="button button--light button--grid-block"><?php echo get_sub_field('button_label') ?></a>
              <?php endif; ?>

            <?php endif; ?>

          </div> <!-- /.wrap -->
        </div> <!-- /.grid-block-section -->

      <?php elseif ( get_row_layout() == 'cards' ): ?>

        <div class="wrap">
          <article class="cards-block">
            <div class="twocol-section body-copy">

              <div class="twocol-section__aside">

                <h2 class="cards-block__title"><?php the_sub_field('title'); ?></h2>

              </div>

              <div class="twocol-section__body">

                <div class="cards-block__excerpt">
                  <?php the_sub_field('excerpt'); ?>
                </div> <!-- /.cards-block__excerpt -->

              </div> <!-- /.twocol-section__body -->

            </div>



            <?php if( have_rows('list') ): ?>
            <ul class="cards-block-list">
            <?php while( have_rows('list') ): the_row(); ?>
              <li class="cards-block-list__item">
                <div class="card">
                  <?php the_sub_field('content'); ?>
                </div>
              </li>
            <?php endwhile; ?>
            </ul> <!-- /.cards-block-list -->
            <?php endif; //if( have_rows('list') ): ?>


          </article> <!-- /.cards-block -->
        </div> <!-- /.wrap -->


        <?php elseif( get_row_layout() == 'profiles' ): ?>
          <div class="wrap wrap--people">
          <div class="people-group">

            <h2 class="people-group__title"><?php the_sub_field('group_title'); ?></h2>

          <?php if( have_rows('profile') ): ?>
              <ul class="people-grid">
              <?php while ( have_rows('profile') ) : the_row(); ?>
                <li class="people-grid__item">
                  <div class="profile">

                    <?php
                    $profile_avatar = get_sub_field('avatar');
                    ?>

                    <?php if ($profile_avatar): ?>

                    <?php else: ?>
                      <img src="<?php echo get_template_directory_uri(); ?>/images/placeholder-avatar.png" alt="" class="profile__avatar"/>
                    <?php endif; ?>

                    <h3 class="profile__name"><?php the_sub_field('name'); ?></h3>
                    <div class="profile__body body-copy">
                      <?php the_sub_field('text'); ?>
                    </div>

                  </div> <!-- profile -->
                </li>

              <?php endwhile; ?>
            </ul>

          <?php else : ?>

          <?php endif; ?>
        </div> <!-- .people-group -->
        </div> <!-- .wrap -->

        <?php elseif ( get_row_layout() == 'text' ): ?>
        <div class="wrap wrap--people">
        <div class="people-group">
          <h2 class="people-group__title"><?php the_sub_field('group_title'); ?></h2>

          <div class="people-group__body body-copy">
            <?php the_sub_field('content'); ?>
          </div>
        </div> <!-- .people-group -->
        </div> <!-- .wrap -->


      <?php endif; ?>

    <?php endwhile; ?>

<?php else : ?>

<?php endif; ?>
