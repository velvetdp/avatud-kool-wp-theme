<div class="wrap">
  <div id="vorm" class="subscribe">
    <h1 class="subscribe__title">Kirjuta meile</h1>
    <p class="subscribe__subtitle">ja ole kursis meie tegemistega</p>

    <div class="subscribe-form">
      <div class="subscribe-form-inner">

        <p class="subscribe-form__title">Tere <strong>Avatud Kool,</strong></p>
        <?php if (is_page('tahan-panustada')) { ?>

          <?php gravity_form( $id_or_title = 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = array('subject_select' => 'abistamisest', ), $ajax = true, $tabindex = 12, $echo = true ); ?>

        <?php } else { ?>

          <?php gravity_form( $id_or_title = 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values, $ajax = true, $tabindex = 12, $echo = true ); ?>

        <?php } ?>

      </div> <!-- /.subscribe-form-inner -->
    </div> <!-- /.subscribe-form -->
  </div>
</div> <!-- /.wrap -->
