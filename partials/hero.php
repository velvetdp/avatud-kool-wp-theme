<?php

if (get_field('large_hero') == true): ?>

<div class="hero" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/front-page-bg.jpg')">

  <div class="wrap">
    <div class="hero-logo-wrap">
      <a href="#" class="hero-logo">Kalamaja Avatud Kool</a>
    </div> <!-- /.hero-inner -->

    <div class="hero-cta">

      <div class="hero-cta__title">
        <h1><?php the_field('hero_title'); ?></h1>
      </div>

      <div class="hero-cta__subtitle">
        <a href="https://www.facebook.com/avatudkool/" class="fb-button"><img src="<?php echo get_template_directory_uri() ?>/images/icon-fb.svg" alt="Facebook" />/avatudkool</a>
        <p><?php the_field('hero_subtitle'); ?></p>
        <!-- <a href="#skip" class="button button--hero button--arrow-down js-scroll-nav">Loe lähemalt</a> -->
      </div>

    </div> <!-- /.hero-cta -->

  </div>

</div> <!-- /.hero -->
<?php else: ?>
<div class="hero hero--small" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/front-page-bg.jpg')">

  <div class="wrap">

      <!-- <h1 class="blog-title"><?php the_title(); ?></h1> -->

    <div class="hero-logo-wrap">
      <a href="#" class="hero-logo">Kalamaja Avatud Kool</a>
    </div> <!-- /.hero-inner -->
  </div>

</div> <!-- /.hero -->

<?php endif; ?>
