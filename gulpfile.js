// Load plugins
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefix = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber');


// Task that compiles scss files down to good old css
gulp.task('pre-process', function(){
    var onError = function(err) {
      notify.onError({
          title:    "Gulp",
          subtitle: "Failure",
          message:  "Error: <%= error.message %>",
          sound:    "Beep"
      })(err);

      this.emit('end');
    };

  gulp.src('./styles/sass/global.scss')
    .pipe(plumber({errorHandler: onError}))
    .pipe(sourcemaps.init())
    .pipe(sass({ style: 'expanded' }))
    .pipe(prefix())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('styles/css'))
    .pipe(notify({ // Add gulpif here
    title: 'Gulp',
    subtitle: 'Sass task',
    message: 'Success'
  }));
});

gulp.task('default', ['pre-process'], function(){
  gulp.watch('styles/sass/**/*.scss', ['pre-process']);
});
