(function( $ ) {

  var ScrollToSubscribe = {
    init: function() {
      $(window).load(function() {
        if(window.location.hash == '#vorm') {

          // cancel default behaviour
          setTimeout(function() {
            window.scrollTo(0, 0);
          }, 1);

          $('html, body').animate({
            scrollTop: $('#vorm').offset().top
          }, 1000);

        }
      });
    }
  }; // ScrollToSubscribe

  var RegFormTranslation = {
    init: function() {
      if ( $('html').attr('lang') === 'ru-RU' ) {

        $('.subscribe-form-inner').css('max-width', '835px');

        jQuery(document).bind('gform_post_render', function(){

            // Subscribe Form
            // ––––––––––––––––––––––––––––––––––

            // Kirjuta meile
            $('h1.subscribe__title').html('Напишите нам');
            // ja ole kursis meie tegemistega
            $('p.subscribe__subtitle').html('и будьте в курсе наших событий');
            // Tere avatud kool
            $('p.subscribe-form__title').html('Здравствуй, Открытая школа');
            // Mina olen
            $('li#field_1_6').html('Я')
            $('select#input_1_1 option[value=lapsevanem]').html('родитель');
            $('select#input_1_1 option[value=õpetaja]').html('учитель');
            $('select#input_1_1 option[value="haridusvaldkonna ekspert"]').html('эксперт сферы образования');
            $('select#input_1_1 option[value=huviline]').html('заинтересованное');
            // Huvitatud osa
            $('li#field_1_7').html('и желаю получить информацию о')
            $('select#input_1_2 option[value="eelaasta koolist"]').html('подготовительной школе');
            $('select#input_1_2 option[value="abistamisest"]').html('возможности стать волонтёром');
            $('select#input_1_2 option[value="õpetajakoolitusest"]').html('курсах для учителей');
            $('select#input_1_2 option[value="lapsevanematekoolist"]').html('школе для родителей');
            $('select#input_1_2 option[value="avatavatest klassidest"]').html('открываемых классах');
            $('select#input_1_2 option[value="õppekavast"]').html('программе обучения');
            $('select#input_1_2 option[value="õppekorraldusest"]').html('организации обучения');
            $('select#input_1_2 option[value="kooli loomisest üldiselt"]').html('создании школы в общем');
            $('select#input_1_2 option[value="tööpakkumistest"]').html('предложениях о работе');
            $('select#input_1_2 option[value="muu"]').html('другом');

            // nimi
            $('li#field_1_9').html('Мои имя и фамилия');

            // email
            $('li#field_1_10').html(', адрес эл. почты');
            $('li#field_1_11').html('.');
            $('li#field_1_12').html('Общаться предпочитаю на');

            //keel
            $('select#input_1_5 option[value="eesti keelt"]').html('эстонском языке');
            $('select#input_1_5 option[value="vene keelt"]').html('русском языке');
            $('select#input_1_5 option[value="inglise keelt"]').html('английском языке');

            //saada
            $('input#gform_submit_button_1').attr('value','Отправить сообщение');

            // Reg Form
            // ––––––––––––––––––––––––––––––––––

            // Lapsevanema nimi<span class="gfield_required">*</span>
            $('label[for="input_2_1"]').html('Имя родителя<span class="gfield_required">*</span>')

            // E-post<span class="gfield_required">*</span>
            $('label[for="input_2_2"]').html('Эл. адрес<span class="gfield_required">*</span>')

            // Telefon<span class="gfield_required">*</span>
            $('label[for="input_2_3"]').html('Телефон<span class="gfield_required">*</span>')

            // Suhtluskeel<span class="gfield_required">*</span>
            $('label[for="input_2_4"]').html('Язык общения (эст, рус, англ)<span class="gfield_required">*</span>')

            // Lapse nimi<span class="gfield_required">*</span>
            $('label[for="input_2_5"]').html('Имя ребенка<span class="gfield_required">*</span>')

            // Lapse sünnipäev ja sünniaasta<span class="gfield_required">*</span>
            $('label[for="input_2_6"]').html('Дата рождения<span class="gfield_required">*</span>')

            // Lapse emakeel<span class="gfield_required">*</span>
            $('label[for="input_2_7"]').html('Родной язык ребенка<span class="gfield_required">*</span>')

            // Valige Avatud Kooli huvikooli osa, millele soovite registreeruda<span class="gfield_required">*</span>
            $('li#field_2_8 > label').html('ВЫБЕРИТЕ КРУЖОК, В КОТОРЫЙ ЖЕЛАЕТЕ ЗАПИСАТЬ РЕБЁНКА<span class="gfield_required">*</span>')

              // <strong>Mitmekeelne loodusteadlaste ring 3.-5. aastastele</strong><br>Kord nädalas 60 minutit arendavaid mänge, nuputamist ja looduse imede avastamist, uusi sõpru, erinevaid keeli ja uutmoodi õppimise rõõmu. Tunnid toimuvad Energia Avastuskeskuses. <br><strong>Hind 25 eurot kuus</strong>
              $('li#field_2_8 label[for="choice_2_8_0"]').html('<strong>Двуязычный кружок естествознания для детей 3-5 лет</strong> <br>60 минут развивающих игр, головоломок, открытий природных чудес и радости от обучения. Помимо этого — возможность завести новых друзей и изучать языки в неформальной обстановке. Занятия проходят один раз в неделю в познавательном центре «Энергия».<br> <strong>Цена 25 евро в месяц.</strong>')

              // <strong>Mitmekeelne robootika ja teaduse ring 5.-7. aastastele</strong> <br>Kord nädalas 90 minutit teadust, mänge, nuputamist, leiutamist, uusi sõpru, erinevaid keeli ja uutmoodi õppimise rõõmu. Tunnid toimuvad Energia Avastuskeskuses. <br><strong>Hind 35 eurot kuus</strong>
              $('li#field_2_8 label[for="choice_2_8_1"]').html('<strong>Двуязычный кружок науки и робототехники для детей 5-7 лет</strong> <br>90 минут науки, игр, головоломок, изобретений и радости от обучения. Помимо этого — возможность завести новых друзей и изучать языки в неформальной обстановке. Занятия проходят один раз в неделю в познавательном центре «Энергия».<br> <strong>Цена 35 евро в месяц.</strong>')

              // <strong>Mitmekeelne robootika ja teaduse ring 7.-9. aastastele</strong><br>Kord nädalas 90 minutit teadust, mänge, nuputamist, leiutamist, uusi sõpru, erinevaid keeli ja uutmoodi õppimise rõõmu. Tunnid toimuvad Energia Avastuskeskuses. <br><strong>Hind 35 eurot kuus</strong>
              $('li#field_2_8 label[for="choice_2_8_2"]').html('<strong>Двуязычный кружок науки и робототехники для детей 7-9 лет</strong> <br>90 минут науки, игр, головоломок, изобретений и радости от обучения. Помимо этого — возможность завести новых друзей и изучать языки в неформальной обстановке. Занятия проходят один раз в неделю в познавательном центре «Энергия».<br> <strong>Цена 35 евро в месяц.</strong>')

              // <strong>Linnalaager “Igapäevaelu teadus” 5.-9. aastastele</strong> <br>5 ja pool päeva teadust, mänge, nuputamist, leiutamist, uusi sõpru, erinevaid keeli ja uutmoodi õppimise rõõmu. Laager toimub Kultuurikatlas ja selle aias.<br> <strong>Hind 100 eurot</strong>
              $('li#field_2_8 label[for="choice_2_8_3"]').html('<strong>Городской лагерь «Наука в повседневной жизни» для детей 5-9 лет</strong> <br>Пять дней игр, головоломок, изобретений и радости от обучения. Помимо этого — возможность завести новых друзей и изучать языки в неформальной обстановке. Место проведения: культурный центр «Котёл культуры» и территория вокруг него.<br> <strong>Цена 100 евро.</strong>')

              // <strong>Kalamaja Avatud Kooli perepäev 1. oktoober kl 10-13</strong><br>Laupäeva hommik, kus lapsed saavad mängida, leiutada ja teadusest vaimustuda ning lapsevanemad saavad samal ajal kuulda Kalamaja Avatud Koolist ning kuidas toetada oma last kooliks valmistumisel. <br><strong>Perepäev on tasuta. Masksimaalselt saab osaleda 60 perekonda!</strong>
              $('li#field_2_8 label[for="choice_2_8_4"]').html('<strong>Семейный день Открытой Школы Каламая 1-го октября с 10 до 13</strong> <br>Этим субботним утром дети смогут поиграть, заняться изобретением и вдохновиться наукой, а их родители — послушать об Открытой школе и о том, как подготовить ребенка к школе.<br> <strong>Вход свободный, максимально 60 семей.</strong>')

            // <strong>Märkige ära soovitud huvitegevuse aeg ja lapse emakeelest lähtuv rühm</strong>
            $('li#field_2_17').html('Выберите время посещения кружка и группу в зависимости от родного языка ребенка')


            // Mitmekeelne loodusteadlaste ring 3.-5. aastastele<span class="gfield_required">*</span>
            $('li#field_2_11 > label').html('Двуязычный кружок естествознания для детей 3-5 лет<span class="gfield_required">*</span>')

              // Kolmapäeviti kl 16.00-17.00 (alates 5.oktoober) eesti emakeel
              $('li#field_2_11 label[for="choice_2_11_0"]').html('Среда 16.00-17.00 (начиная с 5-го октября) родной язык эстонский 10 мест')

              // Kolmapäeviti kl 16.00-17.00 (alates 5.oktoober) vene emakeel
              $('li#field_2_11 label[for="choice_2_11_1"]').html('Среда 16.00-17.00 (начиная с 5-го октября) родной язык русский 10 мест')


            // Mitmekeelne robootika ja teaduse ring 5.-7. Aastastele
            $('li#field_2_10 > label').html('Двуязычный кружок науки и робототехники для детей 5-7 лет<span class="gfield_required">*</span>')

              // Teisipäev 16.30-18.00 (alates 4.oktoober) eesti emakeel 15 kohta
              $('li#field_2_10 label[for="choice_2_10_0"]').html('Вторник 16.30-18.00 (начиная с 4-го октября) родной язык эстонский 15 мест')
              // Teisipäev 16.30-18.00 (alates 4.oktoober) vene emakeel 15 kohta
              $('li#field_2_10 label[for="choice_2_10_1"]').html('Вторник 16.30-18.00 (начиная с 4-го октября) родной язык русский 15 мест')
              // Laupäev 11.00-12.30 (alates 8.oktoober) eesti emakeel 15 kohta
              $('li#field_2_10 label[for="choice_2_10_2"]').html('Суббота 11.00-12.30 (начиная с 8-го октября) родной язык эстонский 15 мест')
              // Laupäev 11.00-12.30 (alates 8.oktoober) vene emakeel 15 kohta
              $('li#field_2_10 label[for="choice_2_10_3"]').html('Суббота 11.00-12.30 (начиная с 8-го октября) родной язык русский 15 мест')


            // Mitmekeelne robootika ja teaduse ring 7.-9. aastastele
            $('li#field_2_12 > label').html('Двуязычный кружок науки и робототехники для детей 7-9 лет<span class="gfield_required">*</span>')

              // Teisipäeviti kl 14.30-16.00 (alates 4.oktoober) eesti emakeel
              $('li#field_2_12 label[for="choice_2_12_0"]').html('Вторник 14.30-16.00 (начиная с 4-го октября) родной язык эстонский 15 мест')
              // Teisipäeviti kl 14.30-16.00 (alates 4.oktoober) vene emakeel
              $('li#field_2_12 label[for="choice_2_12_1"]').html('Вторник 14.30-16.00 (начиная с 4-го октября) родной язык русский 15 мест')

            // Linnalaager (rühm, aeg, lapse emakeel)
            $('li#field_2_9 > label').html('Городской лагерь<span class="gfield_required">*</span>')

              // 5.-7. aastaste rühm 24.-29.oktoober (eesti emakeel)
              $('li#field_2_9 label[for="choice_2_9_0"]').html('5-7 летние 24-29 октября (родной язык эстонский) 15 мест')
              // 7.-9.aastaste rühm 24.-29.oktoober (eesti emakeel)
              $('li#field_2_9 label[for="choice_2_9_1"]').html('7-9 летние 24-29 октября (родной язык эстонский) 15 мест')
              // 5.-7. aastaste rühm 24.-29.oktoober (vene emakeel)
              $('li#field_2_9 label[for="choice_2_9_2"]').html('5-7 летние 24-29 октября (родной язык русский) 15 мест')
              // 7.-9.aastaste rühm 24.-29.oktoober (vene emakeel)
              $('li#field_2_9 label[for="choice_2_9_3"]').html('7-9 летние 24-29 октября (родной язык русский) 15 мест')


            // Kust saite infot Avatud Kooli huvikooli kohta?
            $('li#field_2_13 > label').html('ОТКУДА ВЫ УЗНАЛИ О НАШЕЙ ШКОЛЕ ПО ИНТЕРЕСАМ?')

            // plakat lasteaias, plakat tänaval, Facebook, muu meedia, tuttavad rääkisid, uudiskiri, muu kanal
            $('li#field_2_13 label[for="choice_2_13_1"]').html('Плакат в садике')
            $('li#field_2_13 label[for="choice_2_13_2"]').html('Плакат на улице')
            $('li#field_2_13 label[for="choice_2_13_3"]').html('ФБ')
            $('li#field_2_13 label[for="choice_2_13_4"]').html('СМИ')
            $('li#field_2_13 label[for="choice_2_13_5"]').html('Знакомые рассказали')
            $('li#field_2_13 label[for="choice_2_13_6"]').html('Рассылка')
            $('li#field_2_13 label[for="choice_2_13_7"]').html('Другой источник')

            // Miks soovite oma lapse panna Avatud Kooli huvikooli?
            $('label[for="input_2_15"]').html('Почему хотите отдать ребенка в школу по интересам Открытой Школы?')

            // Küsimused ja kommentaarid
            $('label[for="input_2_16"]').html('Вопросы и комментарии')

            $('#gform_submit_button_2').attr('value', 'Отправить');

        });

      }
    }
  }


  var Global = {
    init: function() {
      this.scrollNav();
      this.menuTrigger();
      this.svgInjector();
    },


    scrollNav: function() {
      $(".js-scroll-nav").click(function(e) {

        e.preventDefault();
        var scrollDest = $(this).attr('href');
        $('html, body').animate({
          scrollTop: $(scrollDest).offset().top-100
        }, 500);

      });
    },

    menuTrigger: function() {

      $('.js-menu-trigger').on('click', function(event) {
        $('.menu-wrapper').slideToggle('fast');
        $(this).text(function(i, text){
          return text === "Menüü" ? "Sulge" : "Menüü";
        })
      })

    },

    svgInjector: function() {
      var mySVGsToInject = document.querySelectorAll('img.icon');
      SVGInjector(mySVGsToInject);
    }


  }; // Global

  Global.init();
  ScrollToSubscribe.init();
  RegFormTranslation.init();

})( jQuery );
